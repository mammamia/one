using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MazeGenerator : MonoBehaviour
{

    public Vector3 StartCoords = new Vector3(0, 0, 0);
    public int Width = 10;
    public int Height = 10;
    public GameObject PerimWalls;
    public GameObject Walls;
	public GameObject[] Trick;
	public GameObject WayOut;
    public GameObject Floors;
    public bool GenerateMazeOnStartup = true;
    
    private int[,] _wallsToBePlaced;

    public int PassageWidth = 1;

    public int ResolutionWidth = 5;
    public int ResolutionHeight = 5;
	
    private const int N = 0;
    private const int E = 1;
    private const int s = 2;
    private const int W = 3;
    private const int N_E = 4;
    private const int s_E = 5;
    private const int s_W = 6;
    private const int N_W = 7;

    private const int HORIZONTAL = 0;
    private const int VERTICAL = 1;

    private GameObject _holdFloor;
    private GameObject _holderWall;

    private HashSet<String> _blacklist = new HashSet<string>();
    public List<float[]> Rooms = new List<float[]>();  

	void Start ()
	{
	    if (!PerimWalls) PerimWalls = Walls;

	    _holdFloor = GameObject.Find("FloorHolder");
	    _holderWall = GameObject.Find("WallHolder");


        if (GenerateMazeOnStartup)
        {
            LayFloorTiles();
            PlaceBorderWalls();
	        CreateMazeThroughRecusriveDivision();
			PlaceTile(WayOut, Width-2,1,Height-2, _holdFloor);
	    }
	}


    private void PlaceTile(GameObject obj, float x, float y, float z, GameObject parent = null)
    {
        PlaceTile(obj, new Vector3(x * Floors.transform.localScale.x, y, z * Floors.transform.localScale.z), parent);
    }

    private void PlaceTile(GameObject obj, Vector3 coords, GameObject parent = null)
    {
        var t = (GameObject)Instantiate(obj, coords, obj.transform.rotation);
		var rand = Mathf.Floor(Random.Range(0, 20));
        t.gameObject.name = obj.name;
        if (parent != null)
        {
            t.transform.parent = parent.transform;
        }
		if(rand>16 && obj.name=="Wall")
		{
			var  randoz = (int) Mathf.Floor(Random.Range(0, Trick.Length));
			var q = (GameObject)Instantiate(Trick[randoz], coords, obj.transform.rotation);
		}
		if(rand>16 && obj.name=="OuterWalls")
		{
			var  randoz = (int) Mathf.Floor(Random.Range(0, Trick.Length));
			var q = (GameObject)Instantiate(Trick[randoz], coords, obj.transform.rotation);
		}
    }

    private void LayFloorTiles()
    {
        if (Floors == null) return;
        for (var i = 0; i < Width; i++)
        {
            for (var k = 0; k < Height; k++)
            {
                PlaceTile(Floors, StartCoords.x + i, StartCoords.y, StartCoords.z + k, _holdFloor);            
            }
        }
    }

    public List<Vector3> GetBorderWalls()
    {
        var borderWallList = new List<Vector3>();
        var yCoord = StartCoords.y + (PerimWalls.transform.localScale.y / 2) + (Floors.transform.localScale.y / 2);
        for (int i = 0; i < Width; i++)
        {
            borderWallList.Add(new Vector3(i, yCoord, 0));
            borderWallList.Add(new Vector3(i, yCoord, Height - 1));
        }
        for (int i = 1; i < Height - 1; i++)
        {
            borderWallList.Add(new Vector3(0, yCoord, i));
            borderWallList.Add(new Vector3(Width - 1, yCoord, i));
        }
        return borderWallList;
    } 

    private void PlaceBorderWalls()
    {
        if (PerimWalls == null) return;
        GetBorderWalls().ForEach(wallCoord => PlaceTile(PerimWalls, wallCoord.x, wallCoord.y, wallCoord.z, _holderWall));
    }

    private void CreateMazeThroughRecusriveDivision()
    {
        _wallsToBePlaced = new int[Width - 2, Height - 2];

        DivideGrid(new Vector2(0,0), Width-2, Height-2, ChooseDirectionToDivide(Width, Height));

        PlaceWalls();
    }

    private void DivideGrid(Vector2 startPoint, int width, int height, int orientation)
    {
        if (width - 1 <= ResolutionWidth || height - 1 <= ResolutionHeight)
        {
            var floatArray = new float[5];
            floatArray[0] = startPoint.x * Floors.transform.localScale.x;
            floatArray[1] = startPoint.y * Floors.transform.localScale.y;
            floatArray[2] = Floors.transform.localScale.z;
            floatArray[3] = width * Floors.transform.localScale.x;
            floatArray[4] = height * Floors.transform.localScale.y;
            Rooms.Add(floatArray);
            return;
        }
        var horizontal = (orientation == HORIZONTAL);
        var wallX = (int) (startPoint.x + (horizontal ? 0 : Mathf.Floor(Random.Range(1, width-2))));
        var wallY = (int) (startPoint.y + (horizontal ? Mathf.Floor(Random.Range(1, height-2)) : 0));
        var length = horizontal ? width : height;
        var direction = horizontal ? E : N;
        var startCoords = new Vector2(wallX, wallY);

        
        DrawWallLine(startCoords,  length, direction);
        CutOutPassageFromWallLine(startCoords, length, direction);

        var newStartPoint = startPoint;
        if (direction == N)
        {
            newStartPoint.y += length;
        }
        else
        {
            newStartPoint.x += length;
        }
        if (horizontal)
        {
            DivideGrid(startPoint, width, (int) (wallY - startPoint.y + 1), ChooseDirectionToDivide(wallX, wallY));
            DivideGrid(new Vector2(startPoint.x, wallY+1), width, (int) (startPoint.y+(height-wallY-1)), ChooseDirectionToDivide(wallX, wallY));
        }
        else
        {
            DivideGrid(startPoint, (int)(wallX - startPoint.x + 1), height, ChooseDirectionToDivide(wallX, wallY));
            DivideGrid(new Vector2(wallX+1, startPoint.y), (int)(startPoint.x+width-wallX-1), height, ChooseDirectionToDivide(wallX, wallY));
        }
    }

    private void CutOutPassageFromWallLine(Vector2 start, int length, int direction)
    {
        var newCoords = start;
        if (direction == N)
        {
            newCoords.y = Mathf.Floor(Random.Range(newCoords.y, length + newCoords.y));
        }
        else
        {
            newCoords.x = Mathf.Floor(Random.Range(newCoords.x, length + newCoords.x));
        }
        var tile = GetTile(newCoords);

        CutPassage(newCoords, direction);
    }

    private void CutPassage(Vector2 coords, int direction)
    {
        _wallsToBePlaced[(int)coords.x, (int)coords.y] = 0;
        BlacklistPassage(coords, direction);
        
        if (PassageWidth > 1)

        {
            var otherCoords = new Vector3(coords.x, coords.y, 0);
            var cutDirection = direction;
            for (var i = 0; i < PassageWidth; i++)
            {
                otherCoords = GetTile(new Vector2(otherCoords.x, otherCoords.y), cutDirection);
                if ((int)otherCoords.z == -1)
                {
                    cutDirection = ChangeDirection(cutDirection);
                    otherCoords.x = coords.x;
                    otherCoords.y = coords.y;
                    if (cutDirection == direction) return;
                    continue;
                }
                if ((int) otherCoords.z == 0)
                {
                    continue;
                }
                _wallsToBePlaced[(int)otherCoords.x, (int)otherCoords.y] = 0;
                BlacklistPassage(new Vector2(otherCoords.x, otherCoords.y), direction);
            }
        }
    }

    public int ChangeDirection(int currentDir)
    {
        if (currentDir == N) return s;
        if (currentDir == s) return N;
        if (currentDir == E) return W;
        if (currentDir == W) return E;
        if (currentDir == N_E) return s_W;
        if (currentDir == N_W) return s_E;
        if (currentDir == s_E) return N_W;
        if (currentDir == s_W) return N_E;
        return N;
    }

    private void BlacklistPassage(Vector2 point, int perpendicularDirection)
    {
        _blacklist.Add(point.x+"-"+point.y);
        if (perpendicularDirection == E || perpendicularDirection == W)
        {
            var otherPoint = GetTile(point, N);
            if ((int) otherPoint.z != -1)
                _blacklist.Add(otherPoint.x + "-" + otherPoint.y);
            otherPoint = GetTile(point, s);
            if ((int) otherPoint.z != -1)
                _blacklist.Add(otherPoint.x + "-" + otherPoint.y);
        }
        else
        {
            var otherPoint = GetTile(point, E);
            if ((int)otherPoint.z != -1)
                _blacklist.Add(otherPoint.x + "-" + otherPoint.y);
            otherPoint = GetTile(point, W);
            if ((int)otherPoint.z != -1)
                _blacklist.Add(otherPoint.x + "-" + otherPoint.y);
        }
    }

    private bool IsBlacklisted(Vector2 point)
    {
        return _blacklist.Contains(point.x + "-" + point.y);
    }

    private void DrawWallLine(Vector2 start, int length, int direction)
    {
        var tile = _wallsToBePlaced[(int)start.x, (int)start.y];
        var currentCoords = start;
        for (var i = 0; i < length; i++)
        {
            if (tile == 1) continue;
            _wallsToBePlaced[(int)currentCoords.x, (int)currentCoords.y] = 1;
            var nextTile = GetTile(currentCoords, direction);
            tile = (int)nextTile.z;
            currentCoords = new Vector2(nextTile.x, nextTile.y);
        }
    }

    private int ChooseDirectionToDivide(int width, int height)
    {
        if (width > height) return HORIZONTAL; //0 == Horizontal
        if (height > width) return VERTICAL; //1 == Vertical
        return (int)Mathf.Floor(Random.Range(0, 2));
    }

    private void PlaceWalls()
    {
        var yCoord = StartCoords.y + (Walls.transform.localScale.y/2) + (Floors.transform.localScale.y/2);
        for (var i = 0; i < Width-2; i++)
        {
            for (var k = 0; k < Height-2; k++)
            {
                if (_wallsToBePlaced[i, k] == 1)
                {
                    if (IsBlacklisted(new Vector2(i, k)))
                    {
                        continue;
                    }
                    PlaceTile(Walls, i + 1, yCoord, k + 1, _holderWall);
                }
            }
        }
    }

    private Vector3 GetTile(Vector2 currentCoords, int direction = -1)
    {
        var returnVector = new Vector3(currentCoords.x, currentCoords.y, 0);
        switch (direction)
        {
            case N: //N : 0
                returnVector.y += 1;
                break;
            case E: //E : 1
                returnVector.x += 1;
                break;
            case s: //South : 2
                returnVector.y -= 1;
                break;
            case W: //West : 3
                returnVector.x -= 1;
                break;
            case N_E: //N-E : 4
                returnVector.y += 1;
                returnVector.x += 1;
                break;
            case s_E: //South-E : 5
                returnVector.y -= 1;
                returnVector.x += 1;
                break;
            case s_W: //South-West : 6
                returnVector.y -= 1;
                returnVector.x -= 1;
                break;
            case N_W: //N-West : 7
                returnVector.y += 1;
                returnVector.x -= 1;
                break;
            default:
                break;
        }
        
        if (returnVector.x > Width - 3 || returnVector.y > Height - 3) returnVector.z = -1;
        else if (returnVector.x < 0 || returnVector.y < 0) returnVector.z = -1;
        else returnVector.z = _wallsToBePlaced[(int)returnVector.x, (int)returnVector.y];

        return returnVector;
    }

	/*private void SpawnWayOut()
	{
		var spawnx = Mathf.Floor(Random.Range(0, 1));
		var spawny = Mathf.Floor(Random.Range(0, 1));
		if (spawnx==1)

		var sp = (GameObject)Instantiate(WayOut, coords, obj.transform.rotation);
	}*/
}
