﻿using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	void OnTriggerEnter(Collider Other )
	{
		Application.Quit();
	}
}
