﻿using UnityEngine;
using System.Collections;

public class Anim : MonoBehaviour {
	protected Animator anim;
	public GameObject Char;
	public bool pugno;
	public bool guardone;
	public bool calcio;
	// Use this for initialization
	void Start () {
		Char = GameObject.FindGameObjectWithTag("Player");
		anim = Char.GetComponent<Animator>();
	
	}
	
	void OnTriggerEnter(Collider other) {
		if (pugno == true){
			anim.SetBool("Pugno", true);}
		if (guardone == true){
			anim.SetBool("Guardone", true);}
		if (calcio == true){
			anim.SetBool("Calcio", true);}

	}
	void OnTriggerExit(Collider other) {
		anim.SetBool("Pugno", false);
		anim.SetBool("Calcio", false);
		anim.SetBool("Guardone", false);
	}
}
